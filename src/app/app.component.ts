import { Component } from "@angular/core";

import { Platform } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { AppgainPlugin } from "@ionic-native/appgain-plugin/ngx";
import { Broadcaster } from "@ionic-native/broadcaster/ngx";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private appGain: AppgainPlugin,
    private broadcaster: Broadcaster
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initAppGain();
    });
  }

  initAppGain() {
    this.appGain
      .initSDK(
        "5eeb8b78461ce10012302d11",
        "f3d3397fb2bcc24a94ee243c249f1eb8fe364009a0e691999c6109c43418c4e2",
        true
      )
      .then((success) => {
        console.log({ success });
        this.brodacsterListener();
        this.setUserID();
        console.log("Should Work");
      })
      .catch((error) => {
        console.log({ error });
      });
  }

  brodacsterListener() {
    this.broadcaster.addEventListener("io.appgain.intent.SILENTPUSH").subscribe(
      (event) => console.log(event),
      (err) => {
        console.log({ err });
      }
    );
  }

  setUserID() {
    this.appGain
      .setUserId("emu-01")
      .then((success) => {
        console.log({ success });
      })
      .catch((error) => {
        console.log({ error });
      });
  }
}
